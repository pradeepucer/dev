var UTIL = {
	getCaretPosition: function(oField) {
		// Initialize
		var iCaretPos = 0;

		// IE Support
		if (document.selection) {
			// To get cursor position, get empty selection range
			var oSel = document.selection.createRange();

			// Move selection start to 0th position
			oSel.moveStart("character", 0 - oField.value.length);

			// The caret position is selection length
			iCaretPos = oSel.text.length;
		}
		// Firefox support
		else if (oField.selectionStart || oField.selectionStart == "0") {
			iCaretPos = oField.selectionStart;
		}

		// Return results
		return (iCaretPos);
	},

	setCaretPosition: function(oField, iCaretPos) {
		// IE Support
		if (document.selection) {
			// Create empty selection range
			var oSel = document.selection.createRange();

			// Move selection start and end to 0 position
			oSel.moveStart("character", 0 - oField.value.length);

			// Move selection start and end to desired position
			oSel.moveStart("character", iCaretPos);
			oSel.moveEnd("character", 0);
		}
		// Firefox support
		else if (oField.selectionStart || oField.selectionStart == "0") {
			oField.selectionStart = iCaretPos;
			oField.selectionEnd = iCaretPos;
		}
	},

	empty: function(mixed_var) {
		// Checks if the argument variable is empty
		// undefined, null, false, number 0, empty string,
		// string "0", objects without properties and empty arrays
		// are considered empty
		//
		// http://kevin.vanzonneveld.net
		// +   original by: Philippe Baumann
		// +      input by: Onno Marsman
		// +   bugfixed by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
		// +      input by: LH
		// +   improved by: Onno Marsman
		// +   improved by: Francesco
		// +   improved by: Marc Jansen
		// +      input by: Stoyan Kyosev (http://www.svest.org/)
		// +   improved by: Rafal Kukawski
		// *     example 1: empty(null);
		// *     returns 1: true
		// *     example 2: empty(undefined);
		// *     returns 2: true
		// *     example 3: empty([]);
		// *     returns 3: true
		// *     example 4: empty({});
		// *     returns 4: true
		// *     example 5: empty({'aFunc' : function () { alert('humpty'); } });
		// *     returns 5: false
		var undef, key, i, len;
		var emptyValues = [undef, null, false, 0, "", "0"];

		for (i = 0, len = emptyValues.length; i < len; i++) {
			if (mixed_var === emptyValues[i]) {
				return true;
			}
		}

		if (typeof mixed_var === "object") {
			for (key in mixed_var) {
				// TODO: should we check for own properties only?
				//if (mixed_var.hasOwnProperty(key)) {
				return false;
				//}
			}
			return true;
		}

		return false;
	},

	popupWindow: function(url, w, h) {
		//window.open(url,'popupWindow','toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=no,resizable=yes,copyhistory=no,width='+w+',height='+h+',screenX=150,screenY=150,top=150,left=150')
		window.open(url, 'popupWindow', 'scrollbars=yes,resizable=yes,copyhistory=no,width=' + w + ',height=' + h + ',screenX=150,screenY=150,top=150,left=150')
	},

	dump: function(arr, level) {
		var dumped_text = "";
		if (!level) level = 0;

		//The padding given at the beginning of the line.
		var level_padding = "";
		for (var j = 0; j < level + 1; j++) level_padding += "    ";

		if (typeof(arr) == 'object') { //Array/Hashes/Objects
			for (var item in arr) {
				var value = arr[item];

				if (typeof(value) == 'object') { //If it is an array,
					dumped_text += level_padding + "'" + item + "' ...\n";
					dumped_text += dump(value, level + 1);
				} else {
					dumped_text += level_padding + "'" + item + "' => \"" + value + "\"\n";
				}
			}
		} else { //Stings/Chars/Numbers etc.
			dumped_text = "===>" + arr + "<===(" + typeof(arr) + ")";
		}
		return dumped_text;
	},

	getParameterByName: function(name, url) {
		if (!url) {
			url = window.location.href;
		}
		name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
		var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
			results = regex.exec(url);
		return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
	},

	randomString: function(length, chars) {
		chars = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
		var result = '';
		for (var i = length; i > 0; --i) result += chars[Math.round(Math.random() * (chars.length - 1))];
		return result;
	},

	updateQueryStringParameter: function(uri, key, value) {
		var re = new RegExp("([?&])" + key + "=.*?(&|$)", "i");
		var separator = uri.indexOf('?') !== -1 ? "&" : "?";
		if (uri.match(re)) {
			return uri.replace(re, '$1' + key + "=" + value + '$2');
		} else {
			return uri + separator + key + "=" + value;
		}
	},

	getSelectionText: function() {
		var text = "";
		if (window.getSelection) {
			text = window.getSelection().toString();
		} else if (document.selection && document.selection.type != "Control") {
			text = document.selection.createRange().text;
		}
		return text;
	},

	highlightCurrentMenu: function(selector, url_param) {
		//highlight current / active link
		var url = window.location.href;
		var a_url = url.match(new RegExp("[^?]+"));
		$(selector).each(function() {
			var m_url = $($(this))[0].href;
			var prj = UTIL.getParameterByName(url_param, m_url)

			var a_m_url = m_url.match(new RegExp("[^?]+"));
			a_m_url = String(a_m_url);
			a_url = String(a_url);
			if ($($(this))[0].href == String(window.location)) {
				$(this).parent().addClass('active');
			}
			/*else if( String(a_m_url) == String(a_url)  ) {
			           $(this).parent().addClass('active');
			       }*/
			/*else if( a_url.indexOf(a_m_url) !=-1  ) {
			           $(this).parent().addClass('active');
			       }*/

		});
	},

    /*    EXPMPLE

    var data = <?php echo json_encode($files); ?> ;

    window.onload = function() {
        var callback = {
            path_url : function(key,val,row){
                return `<a href=${val}>${key}</a>`;

            }
        }
       // document.querySelector('.resp').innerHTML = loop_json_array(data, '<li>{path}, {path_url}, {name}</li>',callback);
        document.querySelector('.resp').innerHTML = loop_json_array(data, '<li>[path], [path_url], [name]</li>',callback,{'start':'[','end':']'});
    }



    */
    loop_json_array: function (data, single_item, callback={},bound={'start':'{','end':'}'}) {
        var op = '';
        var single = '';
        //console.log(data)
        data.forEach((val, key) => {
            // console.log(val)
            single = single_item;
            Object.entries(val).forEach(([k, v]) => {
                if( typeof callback[k] !== "undefined"){
                    var value = callback[k](k,v,val);
                } else {
                    var value = v;
                }
               // console.log('{' + k + '}')
                var rep = bound.start+k+bound.end;
                var regex = new RegExp(rep, 'g');
                single = single.replace(regex,value);
                //single = single.replace(k, v);

            })
            op += single;
        })

        return op;
    },
    loop_json: function (data, single_item, callback={},bound={'start':'{','end':'}'}) {
        var op = '';
        var single = '';
        data.forEach((val, key) => {
            // console.log(val)
            single = single_item;
            Object.entries(val).forEach(([k, v]) => {
                if( typeof callback[k] !== "undefined"){
                    var value = callback[k](k,v,val);
                } else {
                    var value = v;
                }
                console.log('{' + k + '}')
                var rep = bound.start+k+bound.end;
                single = single.replace(rep,value);
                //single = single.replace(k, v);

            })
            op += single;
        })

        return op;
    },

    replace_placeholder: function (data,string,bound={'start':'{','end':'}'}){
        var op = '';
        var single = '';
        Object.entries(val).forEach(([k, v]) => {
            if( typeof callback[k] !== "undefined"){
                var value = callback[k](k,v,val);
            } else {
                var value = v;
            }
            console.log('{' + k + '}')
            var rep = bound.start+k+bound.end;
            single = single.replace(rep,value);
            //single = single.replace(k, v);

        })
        op += single;
        return op;

    },


}
var AJX = {

	'processing_ajx_request' 	: false,
	'jquery' : true,
    'call' :
    function (evnt, elem, url, callback,params) {
	evnt.preventDefault();
	var conf = $(elem).data('confrimation');
	if (conf) {
		if (!confirm(conf)) {
			return false;
		}
	}


	if (this.processing_ajx_request) {
		return;
	}


	this.processing_ajx_request = true;


	if (!end_point) {
		var url = $(elem).data('url');
	} else {
		var url = SITE_VARS ? SITE_VARS.base_url + end_point : end_point
	}
	jQuery.ajax({
		type: 'GET',
		url: url,
		//processData: false,
		//contentType: false,
		//dataType: 'json',
		data: $(elem).data(),
		success: function(resp) {
			if (typeof(callback) == 'function') {
				callback(resp, elem)
			} else {
				var t = $(elem).data('target');
				$(t).html(resp);
			}
		},
		complete: function() {
			processing_ajx_request = false;
		}
	});
	return;
},
'call_ajax' : function(param){

	jQuery.ajax({
		type: param.type,
		url: param.url,
		context:this,
		//processData: false,
		//contentType: false,
		//dataType: 'json',
		data: param.data,
		success: function(resp) {
			if (typeof(callback) == 'function') {
				callback(resp, elem)
			} else {
				var t = $(elem).data('target');
				$(t).html(resp);
			}
		},
		complete: function() {
			processing_ajx_request = false;
		}
	});

}


}
function empty(mixed_var) {
	var undef, key, i, len;
	var emptyValues = [undef, null, false, 0, "", "0"];

	for (i = 0, len = emptyValues.length; i < len; i++) {
		if (mixed_var === emptyValues[i]) {
			return true;
		}
	}
	if (typeof mixed_var === "object") {
		for (key in mixed_var) {
			//}
		}
		return true;
	}
	return false;
}

function do_ajax_request(evnt, elem, end_point, callback) {
	evnt.preventDefault();
	var conf = $(elem).data('confrimation');
	if (conf) {
		if (!confirm(conf)) {
			return false;
		}
	}


	if (processing_ajx_request) {
		return;
	}


	processing_request = true;


	if (!end_point) {
		var url = $(elem).data('url');
	} else {
		var url = SITE_VARS ? SITE_VARS.base_url + end_point : end_point
	}
	jQuery.ajax({
		type: 'GET',
		url: url,
		//processData: false,
		//contentType: false,
		//dataType: 'json',
		data: $(elem).data(),
		success: function(resp) {
			if (typeof(callback) == 'function') {
				callback(resp, elem)
			} else {
				var t = $(elem).data('target');
				$(t).html(resp);
			}
		},
		complete: function() {
			processing_ajx_request = false;
		}
	});
	return;
}

function save_file_form_data(e, form_id, end_point, callback) {
	e.preventDefault();
	if (processing_ajx_request) {
		return;
	}


	processing_request = true;

	let myForm = document.getElementById(form_id);
	var formData = new FormData(myForm);
	//formData.append('file', $('#profileImage')[0].files[0]);
	//formData.append('post', data );

	if (!end_point) {
		var url = $('#' + form_id).attr('action');
	} else {
		var url = SITE_VARS ? SITE_VARS.base_url + end_point : end_point

	}
	jQuery.ajax({
		type: 'POST',
		url: url,
		processData: false,
		contentType: false,
		//dataType: 'json',
		data: formData,
		success: function(resp) {
			if (typeof(callback) == 'function') {
				callback(resp, elem)
			}
		},
		complete: function() {
			processing_ajx_request = false;
		}
	});
	return;
}

function save_form_data(e, elem, end_point, callback) {
	e.preventDefault();
	if (processing_ajx_request) {
		return;
	}
	processing_request = true;
	var data = $(elem).serialize();
	if (!end_point) {
		var url = $(elem).attr('action');
	} else {
		var url = SITE_VARS ? SITE_VARS.base_url + end_point : end_point
	}
	jQuery.ajax({
		type: 'POST',
		url: url,
		//dataType: 'json',
		data: data,
		success: function(resp) {
			if (typeof(callback) == 'function') {
				callback(resp, elem)
			}
		},
		complete: function() {
			processing_ajx_request = false;
		}
	});
	return;
}

function populate_data(elem) {
	fetch_populate_data(elem).then(resp => {
		/*var map = new Map([
  	  		  ['name', 'company_name'],
  	  		  ['email', 'company_email'],
  	  		  ['phone', 'company_phone'],
  	  		  ['address', 'company_address'],
  	  	  ]);;*/
		var record = resp[0];
		for (let k in record) {
			//var res = '#'+map.get(k);
			var res = '.data_' + k;
			$(res).val(record[k])
		}
		console.log(resp)
	});
	return;
}
async function pre_populate_data(elem) {
	let u = $(elem).data('pupulate-url');
	const response = await fetch(u);
	const resp = await response.json();
	return resp;
}

function popluate_json_data(json_data, selector) {
	if (!selector) {
		selector = '.placeholder'
	}

	var placeholderElements = document.querySelector(selector);
	for (var j = 0; j < placeholderElements.length; j++) {
		var elem = placeholderElements[j];
		// see if text content matches [keyName]
		var key_name = elem.getAttribute('data-placeholder');
		if (typeof json_data.key_name !== "undefined") {
			elem.innerHTML = json_data.key_name;
		}
		/* if(placeholderElements[j].textContent === '[' + keyName + ']') {
          console.log('found element', keyName);
          placeholderElements[j].innerHTML = json_data.;
        break;
      }*/
	}

	return;
}


function call_ajax(e,elem,callback,params){

  return;
}